API Documentation
-----------------

Below are the modules of this package.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules/bitcoding.rst
   modules/bitnoisemodels.rst
