# This file uses NumPy style docstrings: https://github.com/numpy/numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt

"""Unit tests for the ccoding module.

"""
import unittest
from bitarray import bitarray
from netsquid_bitencoding.bitcoding import BitCodedItem, BitCodedBytes, BitCodedInteger,\
    BitCodedString, BitCodingFactory, BitCodedBitarray

TEST_INT = 1234567890
TEST_HEX = 0xFA1AFE1
TEST_BYTES = b"test"
TEST_STRING = "test"
TEST_MAP = {"test": "TEST"}
TEST_LIST = [TEST_INT, TEST_HEX, TEST_BYTES, TEST_STRING, TEST_MAP]
TEST_BITARRAY = bitarray([0, 1, 0, 1, 1, 0])

# TODO add tests for bitarray functionality of BitCodedItem


class TestBitCodedItem(unittest.TestCase):
    def test_init(self):
        o = object()
        c = BitCodedItem(item=o)
        assert c.item == o
        assert not c.has_encoding
        self.assertEqual(c.encoded_item, None)

    def test_encode_decode(self):
        o = object()
        c = BitCodedItem(item=o)
        with self.assertRaises(NotImplementedError):
            c.encode()

        with self.assertRaises(NotImplementedError):
            c.decode([False] * 8)

    def test_check_decodeable(self):
        for i in range(17):
            bitstream = [True] * i
            if i > 0 and i % 8 == 0:
                assert BitCodedItem.check_decodeable(bitstream)
            else:
                assert not BitCodedItem.check_decodeable(bitstream)

    def test_set_bitstream(self):
        # Should be possible to manually set bitstream for any coded object
        o = object()
        c = BitCodedItem(item=o)
        c.encoded_item = TEST_BITARRAY
        self.assertEqual(c.encoded_item, TEST_BITARRAY)


class TestBitCodedBytes(unittest.TestCase):
    def test_init(self):
        c = BitCodedBytes(item=TEST_BYTES)
        self.assertEqual(c.encoded_item, c.encode())

        # Test instantiation with mismatching type
        with self.assertRaises(TypeError):
            BitCodedBytes(item=TEST_STRING)

        with self.assertRaises(TypeError):
            BitCodedBytes(item=TEST_INT)

        with self.assertRaises(TypeError):
            BitCodedBytes(item=TEST_LIST)

        with self.assertRaises(TypeError):
            BitCodedBytes(item=TEST_MAP)

    def test_encode_decode(self):
        c = BitCodedBytes(item=TEST_BYTES)
        bitstream = c.encode()
        assert bitstream == [False, True, True, True, False, True, False, False,  # t
                             False, True, True, False, False, True, False, True,  # e
                             False, True, True, True, False, False, True, True,   # s
                             False, True, True, True, False, True, False, False]  # t

        assert len(bitstream) % 8 == 0
        assert c.decode(bitstream) == TEST_BYTES
        assert c.decode() == TEST_BYTES


class TestBitCodedInteger(unittest.TestCase):
    def test_init(self):
        c = BitCodedInteger(item=TEST_INT)
        assert c.item == TEST_INT
        assert c.has_encoding
        self.assertEqual(c.encoded_item, c.encode())

        # Test with hex represented int as well
        c = BitCodedInteger(item=TEST_HEX)
        assert c.item == TEST_HEX
        assert c.has_encoding

        # Test instantiation with mismatching type
        with self.assertRaises(TypeError):
            BitCodedInteger(item=TEST_STRING)

        with self.assertRaises(TypeError):
            BitCodedInteger(item=TEST_BYTES)

        with self.assertRaises(TypeError):
            BitCodedInteger(item=TEST_LIST)

        with self.assertRaises(TypeError):
            BitCodedInteger(item=TEST_MAP)

    def test_to_from_bits(self):
        c = BitCodedInteger(item=TEST_HEX)
        bitstream = c.encode()

        # Padded representation of 0xFA1AFE1 is 0x0FA1AFE1
        assert bitstream == [False, False, False, False, True, True, True, True,   # 0x0F
                             True, False, True, False, False, False, False, True,  # 0xA1
                             True, False, True, False, True, True, True, True,     # 0xAF
                             True, True, True, False, False, False, False, True]   # 0xA1
        assert len(bitstream) % 8 == 0
        assert c.decode(bitstream) == TEST_HEX
        assert c.decode() == TEST_HEX


class TestBitCodedBitarray(unittest.TestCase):
    def test_init(self):
        c = BitCodedBitarray(item=TEST_BITARRAY)
        assert c.item == TEST_BITARRAY
        assert c.has_encoding
        self.assertEqual(c.encoded_item, c.encode())

        # Test instantiation with mismatching type
        with self.assertRaises(TypeError):
            BitCodedString(item=TEST_INT)

        with self.assertRaises(TypeError):
            BitCodedString(item=TEST_BYTES)

        with self.assertRaises(TypeError):
            BitCodedString(item=TEST_LIST)

        with self.assertRaises(TypeError):
            BitCodedString(item=TEST_MAP)

    def test_to_from_bits(self):
        c = BitCodedBitarray(item=TEST_BITARRAY)
        bitstream = c.encode()
        assert bitstream == TEST_BITARRAY

        assert c.decode(bitstream) == TEST_BITARRAY
        assert c.decode() == TEST_BITARRAY


class TestBitCodedString(unittest.TestCase):
    def test_init(self):
        c = BitCodedString(item=TEST_STRING)
        assert c.item == TEST_STRING
        assert c.has_encoding
        self.assertEqual(c.encoded_item, c.encode())

        # Test instantiation with mismatching type
        with self.assertRaises(TypeError):
            BitCodedString(item=TEST_INT)

        with self.assertRaises(TypeError):
            BitCodedString(item=TEST_BYTES)

        with self.assertRaises(TypeError):
            BitCodedString(item=TEST_LIST)

        with self.assertRaises(TypeError):
            BitCodedString(item=TEST_MAP)

    def test_to_from_bits(self):
        c = BitCodedString(item=TEST_STRING)
        bitstream = c.encode()
        assert bitstream == [False, True, True, True, False, True, False, False,  # t
                             False, True, True, False, False, True, False, True,  # e
                             False, True, True, True, False, False, True, True,   # s
                             False, True, True, True, False, True, False, False]  # t

        assert len(bitstream) % 8 == 0
        assert c.decode(bitstream) == TEST_STRING


class TestBitCodingFactory(unittest.TestCase):
    def test_factory(self):
        f = BitCodingFactory()
        c = f.create_coded_item(item=TEST_INT)
        assert isinstance(c, BitCodedInteger)

        c = f.create_coded_item(item=TEST_BYTES)
        assert isinstance(c, BitCodedBytes)

        c = f.create_coded_item(item=TEST_STRING)
        assert isinstance(c, BitCodedString)

        c = f.create_coded_item(item=TEST_LIST)
        assert isinstance(c, BitCodedItem)

        c = f.create_coded_item(item=TEST_MAP)
        assert isinstance(c, BitCodedItem)

        c = f.create_coded_item(item=TEST_BITARRAY)
        assert isinstance(c, BitCodedItem)


if __name__ == "__main__":
    unittest.main()
