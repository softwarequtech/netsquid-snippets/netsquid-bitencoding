# This file uses NumPy style docstrings: https://github.com/numpy/numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt

"""Unit tests for the cchannel module.

"""
import abc
import math
import unittest
from unittest.mock import patch
import netsquid as ns
from bitarray import bitarray
import netsquid.components.tests.test_channel as test_channel
import netsquid.qubits.qubitapi as qapi
from netsquid.components.models.delaymodels import FixedDelayModel
from netsquid.components.models.cerrormodels import ClassicalErrorModel
from netsquid.components.cchannel import ClassicalChannel
from netsquid.qubits.qubit import Qubit
from netsquid.util import simtools
from netsquid_bitencoding.bitcoding import BitCodingFactory


class LossModel(ClassicalErrorModel, metaclass=abc.ABCMeta):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.properties["rng"] = simtools.get_random_state()
        self.properties.update(kwargs)

    @property
    def rng(self):
        """ :obj:`~numpy.random.RandomState`: Random number generator."""
        return self.properties['rng']

    @rng.setter
    def rng(self, value):
        self.properties['rng'] = value

    def error_operation(self, items, delta_time=0, **kwargs):
        self.apply_loss(items, delta_time=delta_time, **kwargs)

    def apply_loss(self, items, delta_time=0, **kwargs):
        for idx, item in enumerate(items):
            if item is None:
                continue
            prob_loss = self.prob_item_lost(item, delta_time, **kwargs)
            is_qubit = isinstance(item, Qubit)
            if is_qubit and item.is_number_state:
                # If qubit is a number state, then we want to amplitude dampen
                # towards |0> but not physically lose it.
                qapi.amplitude_dampen(item, gamma=prob_loss, prob=1.)
            elif math.isclose(prob_loss, 1.) or self.rng.random_sample() <= prob_loss:
                if is_qubit and item.qstate is not None:
                    qapi.discard(item)
                items[idx] = None


class ThresholdLossModel(LossModel):
    """A test loss model. Loses a bit if it's been on channel
    for longer than threshold time.

    """

    def __init__(self, threshold):
        super().__init__()
        self.threshold = threshold

    def prob_item_lost(self, qubit, delta_time, **kwargs):
        return 1. if delta_time > self.threshold else 0


class AlternatingLossModel(LossModel):
    """A test loss model. Loses every second item independent of time."""

    def __init__(self):
        super().__init__()
        self.lost_last = False

    def prob_item_lost(self, item, delta_time, **kwargs):
        self.lost_last = not self.lost_last
        return not self.lost_last


class ThresholdFrameNoiseModel(ClassicalErrorModel):
    """A test noise model based on a delay threshold.

    Replace items with a noisy item if they've been on channel
    for longer than threshold time.

    """

    def __init__(self, threshold, noisy_item="NoisyFrame"):
        super().__init__()
        self.threshold = threshold
        self.noisy_item = noisy_item

    def error_operation(self, items, delta_time=0, **kwargs):
        if delta_time > self.threshold:
            for i, item in enumerate(items):
                items[i] = self.noisy_item


class ThresholdBitNoiseModel(ClassicalErrorModel):
    """A test noise model based on a delay threshold.

    Flip all bits if it they've been on channel for longer
    than threshold time.

    """

    def __init__(self, threshold):
        super().__init__()
        self.threshold = threshold

    def error_operation(self, items, delta_time=0, **kwargs):
        if delta_time > self.threshold:
            for i, bit in enumerate(items):
                items[i] = not bit


class TestClassicalChannel(test_channel.TestChannel):
    """Unit tests for the cchannel module.

    """

    def setUp(self):
        """Sets up a new simulation environment for each test."""
        super().setUp()
        self.test_items = [
            "cat",  # encodable
            b"dog",  # encodable
            1024,  # encodable
            bitarray([0, 1, 0, 0, 1, 1, 1]),  # encodable
            {"foo": "bar"},  # not encodable
        ]
        self.Channel_cls = ClassicalChannel

    def test_ClassicalChannel_init(self):
        """Test initialization of ClassicalChannel"""
        delay = 5
        cc = ClassicalChannel("ClassicalChannelTest", delay=delay)
        self.assertEqual(cc.models["classical_noise_model"], None)
        self.assertEqual(cc.models["classical_loss_model"], None)
        self.assertEqual(cc.classical_code, None)
        self.assertTrue(isinstance(cc.models["delay_model"], FixedDelayModel))
        self.assertAlmostEqual(cc.models["delay_model"](), delay)
        delay_model = FixedDelayModel(delay)
        noise_model = ThresholdFrameNoiseModel(3)
        loss_model = ThresholdLossModel(3)
        classical_code = BitCodingFactory()
        cc = ClassicalChannel("ClassicalChannelTest",
                              models={'delay_model': delay_model,
                                      'classical_noise_model': noise_model,
                                      'classical_loss_model': loss_model},
                              classical_code=classical_code)
        self.assertEqual(cc.models["classical_noise_model"], noise_model)
        self.assertEqual(cc.models["classical_loss_model"], loss_model)
        self.assertEqual(cc.classical_code, classical_code)
        self.assertEqual(cc.models["delay_model"], delay_model)
        self.assertAlmostEqual(cc.models["delay_model"](), delay)

    def test_noise_model(self):
        """Test classical noise model on (unencoded) items."""
        # Test an alternating loss model on input bitarrays
        noise_model = ThresholdFrameNoiseModel(5)
        cc = ClassicalChannel("ClassicalChannelTest", models={"classical_noise_model": noise_model})
        # Test noise below threshold
        cc.models["delay_model"] = FixedDelayModel(4)
        self._wait_for_channel_items(cc)
        self._send_item_on_channel(cc, self.test_items[0], 0)
        ns.sim_run()
        self.assertEqual(len(self.read_items), 1)
        self.assertEqual(self.read_items[0], self.test_items[0])
        # Test noise above threshold
        cc.models["delay_model"] = FixedDelayModel(7)
        self._wait_for_channel_items(cc)
        self._send_item_on_channel(cc, self.test_items[0], 0)
        ns.sim_run()
        self.assertEqual(len(self.read_items), 1)
        self.assertEqual(self.read_items[0], noise_model.noisy_item)

    def test_loss_model(self):
        """Tests classical loss models on (unencoded) items."""
        # Test a threshold loss model
        cc = ClassicalChannel("ClassicalChannelTest",
                              models={"classical_loss_model": ThresholdLossModel(5)})
        for delay, read_count in [(3, 1), (7, 0)]:
            cc.models["delay_model"] = FixedDelayModel(delay)
            cc.reset()
            ns.sim_reset()
            # Test the first four items are lost beyond threshold
            for test_item in self.test_items:
                self._wait_for_channel_items(cc)
                self._send_item_on_channel(cc, test_item, 0)
                ns.sim_run()
                self.assertEqual(len(self.read_items), read_count)
                if read_count == 1:
                    self.assertEqual(self.read_items[0], test_item)
        # Test an alternating loss model
        ns.sim_reset()
        cc = ClassicalChannel("ClassicalChannelTest",
                              models={"classical_loss_model": AlternatingLossModel()})
        self._wait_for_channel_items(cc)
        test_items = [0, 1, 0, 0, 1, 1, 1]
        self._send_item_on_channel(cc, test_items, 0)
        ns.sim_run()
        self.assertEqual(len(self.read_items), 1)
        self.assertEqual(self.read_items[0], [0, 0, 1, 1])

    @patch('netsquid.components.cchannel.logger.warning')
    def test_encoding_noise_model(self, mock_warning):
        """Test classical noise model on encoded items."""
        # Test an alternating loss model on input bitarrays
        cc = ClassicalChannel("ClassicalChannelTest",
                              models={"classical_noise_model": ThresholdBitNoiseModel(5)},
                              classical_code=BitCodingFactory())
        input_item = bitarray([0, 1, 0, 0, 1, 1, 1])
        noisy_item = bitarray([1, 0, 1, 1, 0, 0, 0])
        for delay, res_item in [(3, bitarray(input_item)), (7, noisy_item)]:
            cc.models["delay_model"] = FixedDelayModel(delay)
            self._wait_for_channel_items(cc)
            self._send_item_on_channel(cc, input_item, 0)
            ns.sim_run()
            self.assertEqual(len(self.read_items), 1)
            self.assertEqual(self.read_items[0], res_item)
        # Test noise model on non-bitstream input types
        ns.sim_reset()
        cc.reset()
        cc.models["delay_model"] = FixedDelayModel(7)
        # Encodable items:
        for test_item in self.test_items[:4]:
            self._wait_for_channel_items(cc)
            self._send_item_on_channel(cc, test_item, 0)
            ns.sim_run()
            # Check item mangled:
            self.assertEqual(len(self.read_items), 1)
            self.assertNotEqual(self.read_items[0], test_item)
        # Non-encodable item:
        cc.reset()
        self.assertEqual(mock_warning.call_count, 0)
        test_item = self.test_items[4]
        self._wait_for_channel_items(cc)
        self._send_item_on_channel(cc, test_item, 0)
        ns.sim_run()
        self.assertEqual(len(self.read_items), 1)
        self.assertEqual(self.read_items[0], test_item)
        self.assertEqual(mock_warning.call_count, 1)

    @patch('netsquid.components.cchannel.logger.warning')
    def test_encoding_loss_model(self, mock_warning):
        """Tests classical loss models on encoded items."""
        # Test a threshold loss model
        cc = ClassicalChannel("ClassicalChannelTest",
                              models={"classical_loss_model": ThresholdLossModel(5)},
                              classical_code=BitCodingFactory())
        for delay, read_count in [(3, 1), (7, 0)]:
            cc.models["delay_model"] = FixedDelayModel(delay)
            cc.reset()
            ns.sim_reset()
            # Test the first four encodable items are lost beyond threshold
            for test_item in self.test_items[:4]:
                self._wait_for_channel_items(cc)
                self._send_item_on_channel(cc, test_item, 0)
                ns.sim_run()
                self.assertEqual(len(self.read_items), read_count)
                if read_count == 1:
                    self.assertEqual(self.read_items[0], test_item)
            # Test non-encodable item is never lost
            # Should give a warning
            self.read_items = []
            self._send_item_on_channel(cc, self.test_items[4], 0)
            ns.sim_run()
            self.assertEqual(len(self.read_items), 1)
            self.assertEqual(self.read_items[0], self.test_items[4])
            self.assertEqual(mock_warning.call_count, 1)
            mock_warning.call_count = 0  # reset warning count
        # Test an alternating loss model
        ns.sim_reset()
        cc = ClassicalChannel("ClassicalChannelTest",
                              models={"classical_loss_model": AlternatingLossModel()},
                              classical_code=BitCodingFactory())
        self._wait_for_channel_items(cc)
        test_item = bitarray([0, 1, 0, 0, 1, 1, 1])
        self._send_item_on_channel(cc, test_item, 0)
        ns.sim_run()
        self.assertEqual(len(self.read_items), 1)
        self.assertEqual(self.read_items[0], bitarray([0, 0, 1, 1]))


if __name__ == "__main__":
    # ns.logger.setLevel(logging.DEBUG)
    unittest.main()
