# This file uses NumPy style docstrings: https://github.com/numpy/numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt

"""Unit tests for the cnoisemodels module.

"""
import unittest
import logging
import netsquid as ns
from random import choice
from netsquid_bitencoding.bitnoisemodels import BitErrorRateNoiseModel, BurstNoiseModel


class TestBitErrorRateNoiseModel(unittest.TestCase):
    def test_noise_operation(self):
        # Since noise model modifies data in place we need a copy to compare with
        test_bits = [choice([True, False]) for _ in range(1000)]
        test_copy = list(test_bits)

        # Test that error rate of zero leaves the data intact
        ber_model = BitErrorRateNoiseModel(error_rate=0)
        ber_model(items=test_bits)
        assert test_bits == test_copy

        # Test that error rate of one completely inverts data
        ber_model = BitErrorRateNoiseModel(error_rate=1)
        ber_model(items=test_bits)
        for b1, b2 in zip(test_bits, test_copy):
            assert b1 == (not b2)


class TestBurstNoiseModel(unittest.TestCase):
    def test_noise_operation(self):
        # Since noise model modifies data in place we need a copy to compare with
        test_bits = [choice([True, False]) for _ in range(10000)]
        test_copy = list(test_bits)

        # Create a model that we can check (within reason) had proper effect on the bitstream
        burst_model = BurstNoiseModel(error_rate=1, burst_rate=0.001, burst_length=20, burst_overlap=False)
        burst_model(test_bits)

        bit_index = 0
        while bit_index < len(test_bits):
            # Check if we found a burst
            if test_bits[bit_index] == (not test_copy[bit_index]):
                # Verify that all bits within the burst were flipped
                for burst_index in range(burst_model.burst_length):
                    if bit_index + burst_index >= len(test_bits):
                        break

                    assert test_bits[bit_index + burst_index] == (not test_copy[bit_index + burst_index])

                bit_index += burst_model.burst_length

            else:
                assert test_bits[bit_index] == test_copy[bit_index]
                bit_index += 1


if __name__ == "__main__":
    ns.logger.setLevel(logging.DEBUG)
    unittest.main()
