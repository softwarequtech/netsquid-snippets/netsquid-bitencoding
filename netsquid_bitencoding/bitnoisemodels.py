# This file uses NumPy style docstrings: https://github.com/numpy/numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt

"""
Bit noise models allows users to specify custom noise functionality for classical components.

"""

import netsquid.util.simtools as simtools
from netsquid.components.models.cerrormodels import ClassicalErrorModel

__all__ = [
    "BitErrorRateNoiseModel",
    "BurstNoiseModel",
]


class BitErrorRateNoiseModel(ClassicalErrorModel):
    """Generic bit error rate noise model.

    Will randomly flip bits on the channel depending on the given error rate.

    Parameters
    ----------
    error_rate : float
        The rate at which classical bits get flipped.
    classical_code : :obj:`~netsquid_bitencoding.ccoding.ClassicalCodingFactory` or None, optional
        Specifies how the items put on the channel are encoded into the items that
        are sent over the channel.

    Attributes
    ----------
    error_rate : float
        The rate at which classical bits get flipped.

    """

    def __init__(self, error_rate, classical_code=None, **kwargs):
        super().__init__()
        self.properties.update({'error_rate': error_rate})
        self._classical_code = classical_code

    @property
    def error_rate(self):
        """float: the rate at which classical bits get flipped."""
        return self.properties['error_rate']

    @error_rate.setter
    def error_rate(self, value):
        self.properties['error_rate'] = value

    def error_operation(self, items, delta_time=0, **kwargs):
        """Randomly flip bits depending on the given error rate.

        Parameters
        ----------
        items : list of bool
            Bitstream to apply noise to.
        delta_time : float, optional
            Time bitstream has spent on channel [ns].

        """
        # Iterate over the length of the bitstream
        rvec = simtools.get_random_state().random_sample(len(items))
        for i in range(len(items)):
            if rvec[i] <= self.error_rate:
                # We do this to modify the bits in place
                items[i] = 1 - items[i]


class BurstNoiseModel(ClassicalErrorModel):
    """Burst noise model that will randomly flip bits within a burst of the bitstream.

    Parameters
    ----------
    error_rate : float
        The probability a bit within a burst is flipped.
    burst_rate : float
        The probability that a burst begins at a given bit.
    burst_length : float
        The length of burst noise.
    burst_overlap : bool
        Determines whether bursts overlap or not.

    """

    def __init__(self, error_rate, burst_rate, burst_length, burst_overlap, **kwargs):
        # NOTE no longer inherit from BitErrorRateNoiseModel as while
        # both have an error_rate, no implementation in common, and not a child
        super().__init__()
        self.properties.update({
            'error_rate': error_rate,
            'burst_rate': burst_rate,
            'burst_overlap': burst_overlap,
            'burst_length': burst_length
        })

    @property
    def error_rate(self):
        """float: the probability a bit within a burst is flipped."""
        return self.properties['error_rate']

    @error_rate.setter
    def error_rate(self, value):
        self.properties['error_rate'] = value

    @property
    def burst_rate(self):
        """float: the probability that a burst begins at a given bit."""
        return self.properties['burst_rate']

    @burst_rate.setter
    def burst_rate(self, value):
        self.properties['burst_rate'] = value

    @property
    def burst_length(self):
        """float: the length of burst noise."""
        return self.properties['burst_length']

    @burst_length.setter
    def burst_length(self, value):
        self.properties['burst_length'] = value

    @property
    def burst_overlap(self):
        """bool: determines whether bursts overlap or not."""
        return self.properties['burst_overlap']

    @burst_overlap.setter
    def burst_overlap(self, value):
        self.properties['burst_overlap'] = value

    def error_operation(self, items, delta_time=0, **kwargs):
        """Will randomly flip bits within a burst of the bitstream.

        Parameters
        ----------
        items : list of bool
            Bitstream to apply noise to.
        delta_time : float, optional
            Time bitstream has spent on channel [ns].

        """
        bit_index = 0
        while bit_index < len(items):
            # Determine if a burst begins at this bit
            if simtools.get_random_state().random_sample() <= self.burst_rate:
                for burst_index in range(self.burst_length):
                    if bit_index + burst_index >= len(items):
                        return
                    # Determine if a particular bit gets flipped by the burst
                    if simtools.get_random_state().random_sample() <= self.error_rate:
                        items[bit_index + burst_index] = not items[bit_index + burst_index]
                # If we allow bursts to overlap then increment to the next bit
                if self.burst_overlap:
                    bit_index += 1
                # Otherwise move over to the next bit out of the burst length
                else:
                    bit_index += self.burst_length
            # If no burst begins then we go to the next bit
            else:
                bit_index += 1
