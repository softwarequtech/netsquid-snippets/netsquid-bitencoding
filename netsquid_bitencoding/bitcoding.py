# This file uses NumPy style docstrings: https://github.com/numpy/numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt

"""Defines coding utilities for classical components using bits.

"""
from netsquid.components.ccoding import CodedItem, ClassicalCodingFactory
from netsquid.components.models.cerrormodels import ClassicalErrorModel
from netsquid.util.simtools import warn_deprecated
from bitarray import bitarray

__all__ = [
    "BitCodingFactory",
    "BitCodedItem",
    "BitCodedBytes",
    "BitCodedBitarray",
    "BitCodedInteger",
    "BitCodedString"
]


class BitCodedItem(CodedItem):
    """An object used for conversion of item types to bitstreams.

    Subclass for each type of object to implement specific methods
    of encoding/decoding bitstreams.  This base class provides a catch-all
    for generic objects that do not contain a specific encoding scheme.

    Parameters
    ----------
    data : Any
        The object we want to encode into bitstream.

    Notes
    -----
        Based on EasySquid's classicalcoding.py by Matt Skrzypczyk.

    """
    _object_types = (object,)
    _has_encoding = False

    @property
    def has_encoding(self):
        """bool: Whether object has an encoding."""
        return self._has_encoding

    @property
    def object_types(self):
        """tuple of object: Object types that can be converted."""
        return self._object_types

    @property
    def encoding_type(self):
        """type: Object type of encoded item."""
        return bitarray

    def to_bytes(self):
        """Converts data object to a bytestream.

        Returns
        -------
        bytes
            A bytes representation of the object

        """
        raise NotImplementedError("Subclass {} and implement to_bytes".format(type(self)))

    def bits_to_item(self, bits):
        """Convert bitstream to item

        """
        raise NotImplementedError("Subclass {} and implement bits_to_item".format(type(self)))

    def encode(self):
        """General encoding method.

        A specific object implementation simply needs to provide a means of converting to bytes
        and this base class can take care of the rest of conversion to a bitstream

        Returns
        -------
        bitarray
            Object encoded as bitstream.

        """
        bytestream = self.to_bytes()
        bitstream = self.bytes_to_bits(bytestream)
        return bitstream

    def bytes_to_bits(self, bytestream):
        """Converts a bytestream to a bitstream.

        Parameters
        ----------
        bytessream : bytes
            The bytes object we want to convert into a list of bits

        Returns
        -------
        list of bool
            The bitstream representing the object encoding
        """
        bits = bitarray(endian='big')
        bits.frombytes(bytestream)
        return bits.tolist()

    def decode(self, encoded_item=None):
        """Decode a bitstream.

        If no bitstream specified, the locally held bitstream is
        decoded. If no encoding is available, the original data
        is simply returned.

        Parameters
        ----------
        encoded_item : bitarray or None, optional
            Bitstream to decode. If ``None``,
            the locally held bitstream is decoded.

        Returns
        -------
        object or None
            Decoded item.

        """
        if encoded_item is None:
            if not self.has_encoding:
                return self._item
            encoded_item = self.encoded_item
        if self.check_decodeable(encoded_item):
            return self.bits_to_item(encoded_item)
        else:
            raise ValueError("Bitstream {} is not decodeable!".format(encoded_item))

    @staticmethod
    def check_decodeable(bitstream):
        """bool: whether bitstream is decodeable."""
        return len(bitstream) % 8 == 0 and len(bitstream) > 0

    def apply_loss_model(self, loss_model, delta_time=0, **component_properties):
        """Apply a loss model to the encoded item.

        Modifies the encoded item in-place.

        Parameters
        ----------
        loss_model : :obj:`~netsquid.components.models.cerrormodels.ClassicalErrorModel`
            Loss model to apply to encoded item.
        delta_time : float, optional
            Time item has spent on channel [ns].
        component_properties : dict
            Component properties of the component that is transmitting.

        Returns
        -------
        bool
            Whether the whole encoded item is lost.

        """
        if not isinstance(loss_model, ClassicalErrorModel):
            raise TypeError("{} is not a classical error model".format(loss_model))
        # NOTE: bits should be given bit by bit to loss_model, because a loss_model requires a list as input and
        # otherwise treats the items, in this case the bits in a bitarray, as a single item.
        bits = [b for b in self.encoded_item]
        loss_model(bits, delta_time, **component_properties)
        self.encoded_item = bitarray([b for b in bits if b is not None], endian='big')
        return len(self.encoded_item) == 0

    # DEPRECATED methods

    @property
    def bitstream(self):
        """bitarray: encoded bitstream."""
        warn_deprecated("BitCodedItem.bitstream is deprecated. "
                        "Use the BitCodedItem.encoded_item instead.",
                        key="BitCodedItem.bitstream")
        return self.encoded_item

    @bitstream.setter
    def bitstream(self, value):
        warn_deprecated("BitCodedItem.bitstream is deprecated. "
                        "Use the BitCodedItem.encoded_item instead.",
                        key="BitCodedItem.bitstream")
        self.encoded_item = value


class BitCodedInteger(BitCodedItem):
    """Coding scheme for int objects.

    """
    _object_types = (int,)
    _has_encoding = True

    def to_bytes(self):
        # Pads the binary representation to a multiple of 8 for byte conversion
        # Use at least one byte
        length = max((self._item.bit_length() + 7) // 8, 1)
        return self._item.to_bytes(length=length, byteorder='big')

    @staticmethod
    def bits_to_item(bitstream):
        bits = bitarray(initial=bitstream, endian='big')
        return int(bits.to01(), 2)


class BitCodedString(BitCodedItem):
    """Coding scheme for str objects.

    """
    _object_types = (str,)
    _has_encoding = True

    def to_bytes(self):
        return self._item.encode()

    @staticmethod
    def bits_to_item(bitstream):
        bits = bitarray(bitstream, endian='big')
        return bits.tobytes().decode(errors='replace')


class BitCodedBytes(BitCodedItem):
    """Coding scheme for bytes objects.

    """
    _object_types = (bytes,)
    _has_encoding = True

    def to_bytes(self):
        return self._item

    @staticmethod
    def bits_to_item(bitstream):
        bits = bitarray(initial=bitstream, endian='big')
        return bits.tobytes()


class BitCodedBitarray(BitCodedItem):
    """Coding scheme for int objects.

    """
    _object_types = (bitarray,)
    _has_encoding = True

    def to_bytes(self):
        return self._item.tobytes()

    @staticmethod
    def bits_to_item(bitstream):
        return bitarray(bitstream, endian='big')

    def encode(self):
        """General encoding method.

        A specific object implementation simply needs to provide a means of converting to bytes
        and this base class can take care of the rest of conversion to a bitstream

        Returns
        -------
        bitarray
            Object encoded as bitstream.

        """
        # Do nothing
        return bitarray(self._item, endian='big')

    def decode(self, encoded_item=None):
        """Decode a bitstream.

        If no bitstream specified, the locally held bitstream is
        decoded. If no encoding is available, the original data
        is simply returned.

        Parameters
        ----------
        encoded_item : bitarray or None, optional
            Bitstream to decode. If ``None``,
            the locally held bitstream is decoded.

        Returns
        -------
        object
            Decoded data.

        """
        return self.encoded_item


class BitCodingFactory(ClassicalCodingFactory):
    """Class that produces the classical coded item respective of the desired data.

    When introducing new coding schemes they should be added to the ``type_to_coded_item``
    map otherwise the default for types not listed is a BitCodedItem.

    """
    type_to_coded_item = {
        int: BitCodedInteger,
        bytes: BitCodedBytes,
        str: BitCodedString,
        bitarray: BitCodedBitarray,
    }

    @classmethod
    def create_coded_item(cls, item):
        """Creates the classical coded object for given data.

        Parameters
        ----------
        item : Any
            The item we want to encapsulate into a classical coded item.

        Returns
        -------
        :obj:`~netsquid.components.ccoding.BitCodedItem`
            An instantiation of the classical coded object appropriate for the provided item.

        """
        code_class = cls.type_to_coded_item.get(type(item), BitCodedItem)
        return code_class(item=item)
